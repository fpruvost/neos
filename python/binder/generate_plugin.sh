#!/bin/bash

binder=/usr/local/binder/bin/binder
bitpit_include=/usr/local/include/bitpit/
module_name=pyneos
prefix=build
module=${prefix}${module_name}.cpp

# Clean up
rm -f ${prefix}*.cpp
rm -rf ${prefix}
mkdir -p ${prefix}

# Prepare bitpit includes
if [ ! -f ./bitpit/bitpit.hpp ]; then
  mkdir -p ./bitpit
  cp -rf ${bitpit_include} .
  sed  -i 's/include[ ]*"\([^"]*\)"/include <\1>/' bitpit/*
fi

# Generate in multiples files (this will not generate files for std::array et std::vector)
${binder} --bind "" --root-module ${module_name} --prefix ${prefix} -config config Neos.hpp -- -x c++ -std=c++11 -I. -I./bitpit -I../../src/core -I../../src/geometry -I../../src/maths -I../../src/includes -I./bitpit -I/home/gitlab/petsc-3.11.3/arch-linux2-c-debug/include/ -I /home/gitlab/petsc-3.11.3/include/ -I /usr/lib/x86_64-linux-gnu/openmpi/include -DOMPI_SKIP_MPICXX -DBINDER_METHODS -DBITPIT_ENABLE_MPI

# Add iter for Cells (Monkey Patch)
sed -i '1,/assign.*BasePiercedKernel/ {/assign.*BasePiercedKernel/a\
                cl.def("__len__", [](const class bitpit::PiercedVector <bitpit::Cell,long> &v) { return v.size(); });\
                cl.def("__iter__", [](bitpit::PiercedVector <bitpit::Cell,long> &v) {\
                                      return pybind11::make_iterator(v.begin(), v.end());\
                       }, pybind11::keep_alive<0, 1>()); /* Keep vector alive while iterator is used */
}' build/cell.cpp
# remove MPI_struct objects
rm ${prefix}/mpi*
# Concat user files only in main cpp file
cat ${prefix}/*.cpp > ${module}-temp
#rm -f ${prefix}/*.cpp ${prefix}/*.sources ${prefix}/*.modules
mv ${module}-temp ${module}

# Remove references to std::array et std::vector in main cpp file
sed -i '/.*bind_std.*/d' ${module}
sed -i '/.*bind_mpi.*/d' ${module}
sed -i '/.*cl.def( pybind11::init<const .*/d' ${module}
