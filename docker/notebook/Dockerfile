FROM registry.gitlab.inria.fr/memphis/neos:latest

USER root
RUN apt-get update && apt-get install -y apache2 apache2-dev libgl1-mesa-dev xvfb ca-certificates

RUN curl -sL https://deb.nodesource.com/setup_current.x | sudo -E bash - && \
    apt install -y nodejs

COPY fix-permissions /usr/bin
COPY . /tmp/src

RUN rm -rf /tmp/src/.git* && \
    chown -R 1001 /tmp/src && \
    chgrp -R 0 /tmp/src && \
    chmod -R g+w /tmp/src && \
    rm -rf /tmp/scripts && \
    mv /tmp/src/.s2i/bin /tmp/scripts

RUN mkdir -p /opt/app-root/etc /opt/app-root/src /opt/app-root/bin && \
    ln -s /opt/app-root /opt/app-root/src/.local
RUN chmod g=u -R /opt/app-root && chown 1001 -R /opt/app-root
ENV APP_ROOT /opt/app-root
ENV HOME /opt/app-root/src
ENV PATH $PATH:/opt/app-root/bin:/opt/app-root/src/.local/bin
WORKDIR /opt/app-root/src
USER 1001

LABEL io.k8s.description="S2I builder for custom Jupyter notebooks." \
      io.k8s.display-name="Jupyter Notebook" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="builder,jupyter" \
      io.openshift.s2i.scripts-url="image:///opt/app-root/builder"

RUN pwd && ls -al
RUN /tmp/scripts/assemble

CMD [ "/opt/app-root/builder/run" ]
