Neos is an extensible framework, under development, for rapid prototyping of numerical simulations in C++ that can also be used from Python scripts for ease of use for newcomers.

It is based on BitPit lib (octree structure manipulation) and can use different linear parallel solver (like PETSc over MPI).

Neos deals with different kind of objects like :

- neos::Grid : cartesian domain of computation with refinement/coarsening (octree based)
- neos::Geometry : analytical (neos::Sphere, neos::Cylinder, pile of slice) or explicit (neos::STLGeometry)
- neos::LevelSet : distance to neos::Geometry or group of Geometries
- Operators : neos::IInterpolator, neos::Gradient, neos::Laplacian, neos::Transport, Re-distancing
- Solver : Penalization, Neumann/Dirichlet conditions

Results can be easily exported to VTK files (still data or animated).

Neos can also be used in a Jupyter Notebook featuring an embedded VTK  viewer.
